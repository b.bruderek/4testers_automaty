from src.homework_2024_04_15 import check_to_need_vaccinations

@pytest.mark.parametrize("pet_age, pet_kind", "expected", [(1, "cat", True), (2, "cat", False)])
def test_cat_age_one_requires_vaccination(pet_age, pet_kind, expected):
    assert check_to_need_vaccinations(pet_age, pet_kind) == expected