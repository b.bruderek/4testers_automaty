# Napisz funkcję, która będzie generować adresy email w domenie naszej firmy -> “4testers.pl”.
# Email ma formę “imie.nazwisko@4testers.pl”
# Niech funkcja przyjmuje imię i nazwisko i zwraca utworzony email.
# Wydrukuj emaile dla użytkowników:
# ● Janusz Nowak
# ● Barbara Kowalska

def f_generate_email_adress(name,surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"

name = "Barbara"
surname = "Kowalska"
print(f_generate_email_adress(name,surname))