# def map_numbers_to_squared(input_list):
#    output_list = []
#    for number in input_list:
#        output_list.append(number ** 2)
#    return output_list

def map_numbers_to_squared(input_list):
    return [number ** 2 for number in input_list if number % 2 == 0]


test_list = [1, 3, 6, 7, 9, 12, 13, 17, 23, 32]
mapped_list = map_numbers_to_squared(test_list)
print(mapped_list)
