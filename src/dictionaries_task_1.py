# Napisz funkcję, która przyjmuje email, telefon, miasto i ulicę zamieszkania osoby, a następnie zwraca słownik postaci:
def get_person_data_dicionary(email, phone, city, street):
    return {
        "contact": {
            "email": email,
            "phone": phone
        },
        "address": {
            "city": city,
            "street": street
        }
    }
print(get_person_data_dicionary("b.bruderek@tlen.pl", "+48514524414", "Toruń", "Żwirki i Wigury"))
