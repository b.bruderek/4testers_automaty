# Zdefiniuj zmienną friend zawierającą imię, wiek oraz listę dwóch hobby Twojego przyjaciela w postaci słownika.

friend = {
    "name": "Andżej",
    "age": 32,
    "hobbies": ["running", "reading"]
}

print(friend)