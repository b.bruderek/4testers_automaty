class User:
    def __init__(self, email, password):
        self.__email = email
        self.__password = password
        self.__is_logged_in = False

    def login(self, password):
        if password == self.__password:
            self.__is_logged_in = True

    def is_logged_in(self):
        return self.__is_logged_in


user = User("foo@excample.pl", "123Password!@#")
user.login("123Password!@#")
print(user.is_logged_in())
