def prepare_user_payload(email, phone, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }

if __name__ == '__main__':
    pet = {
        "name": "Pushek",
        "kind": "cat",
        "age": 2,
        "weight": 0.5,
        "is_male": True,
        "favourite_food": ["fish", "milk"]
    }

    print(pet["weight"])
    pet["weight"] = 0.2
    pet["likes_swimming"] = False
    del pet["is_male"]
    pet["favourite_food"].append("snacks")

    print(pet)

    payload = prepare_user_payload("email@gmail.com", "123456789")