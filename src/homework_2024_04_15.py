def create_player_description(player_dictionary):
    return f"The player {player_dictionary["nick"]} is of type {player_dictionary["type"]} and has {player_dictionary["exp_points"]} EXP"


def check_to_need_vaccinations(pet_age, pet_kind="else"):
    if pet_age <= 0:
        raise ValueError("Error")
    elif pet_age == 1:
        return True
    elif (pet_age - 1) % 3 == 0 and pet_kind == "cat":
        return True
    elif (pet_age - 1) % 2 == 0 and pet_kind == "dog":
        return True
    else:
        return False


if __name__ == '__main__':
    player = {
        "nick": "maestro54",
        "type": "warrior",
        "exp_points": 3000
    }
    print(create_player_description(player))

    print(check_to_need_vaccinations(1, "horse"))

    pet_age = 9
    print(pet_age % 3)
