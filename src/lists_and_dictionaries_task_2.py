# Dla listy:

emails = ["a@example.com", "b@example.com"]

# Wydrukuj: długość listy

print(len(emails))

# Wydrukuj: pierwszy element

print(emails[0])

# Wydrukuj: ostatni element

print(emails[-1])

# Dodaj nowy email ‘cde@example.com’

emails.append("cde@example.com")
print(emails)
