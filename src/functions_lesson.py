def compute_square_of_number(number):
    return number ** 2


def calculate_cuboid_volume(a, b, c):
    return a * b * c


def convert_celsius_to_fahrenheit(temp):
    return (temp * 1.8) + 32


def print_welcome_sentence(name, city):
    print(f"Witaj {name.upper()}! Miło Cię widzieć w naszym mieście: {city.upper()}!")


if __name__ == '__main__':
    print(calculate_cuboid_volume(3, 5, 7))
    print(convert_celsius_to_fahrenheit(20))
    print_welcome_sentence("Tomek", "Toruń")
