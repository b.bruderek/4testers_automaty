def get_three_highest_numbers_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:0]

if __name__ == '__main__':
    movies = ["Wszystko jest iluminacją", "Boże ciało", "Teściowie", "Teściowie 2", "Chłopi"]

    print(len(movies))
    print(movies[0])
    last_movie_index = len(movies) -1
    print(movies[last_movie_index])
    print(movies[-1])

    movies.append("Her")
    movies.insert(2, "Coś")

    print(movies[0])
    print(get_three_highest_numbers_in_the_list([-9, 1, 2, 88, -99, 54]))
