#def print_each_students_name_capitalized(list_of_students_first_names):
#    for first_name in list_of_students_first_names:
#        print(first_name.capitalize())

#def print_first_three_integers_squared():
#    for integer in range (1,4):
#        print(integer ** 2)

def print_ten_numbers():
    for i in range(10):
        print(i)

def print_even_numbers_in_range():
    for a in range(0, 21, 2):
        print(a)

def print_numbers_from_one_to_thirty_divisible_by_seven():
    for b in range(1,31):
        if b % 7 == 0:
            print(b)

def print_numbers_divisible_by_number_in_range(start, end, divider):
    for c in range(start, end):
        if c % divider == 0:
            print(c)

def generate_list_of_10_random_number_from_1000_to_5000():
    numbers = []
    for i in range(10):
        numbers.append(random.randint(1000, 5000))
    return numbers

temp_celsius = [
def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32
def convert_list_of_celsius_to_list_of_fahrenheit(celsius_list):
    fahrenheit_list = []
    for temperature in celsius_list:
        temperature_in_fahrenheit = convert_celsius_to_fahrenheit(temperature)
        fahrenheit_list.append(temperature_in_fahrenheit)
    return fahrenheit_list

if __name__ == '__main__':
#    print_ten_numbers()
#    print_even_numbers_in_range()
#   print_numbers_from_one_to_thirty_divisible_by_seven()
#    print_numbers_divisible_by_number_in_range(1, 20, 5)