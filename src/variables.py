friends_name = 'Andżej'
friends_age = 32
friends_pet_amount = 0
has_driving_license = True
friendship_years = 5.5

print("Name:", friends_name, sep="\t")
print("Age:", friends_age, sep="\t")
print("Pet amount:", friends_pet_amount, sep="\t")
print("Has driving license:", has_driving_license, sep="\t")
print("Friendship years:", friendship_years, sep="\t")

print("-----")

print(
    "Name:", friends_name,
    "Age:", friends_age,
    sep="\n"
)

print(
    "Name:", friends_name,
    "Age:", friends_age,
    sep="\n"
)

name_surname = "Błażej Bruderek"
email = "blazej.bruderek@gmail.com"
phone_number = "+48123456789"

bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone: " + phone_number
print(bio)

bio_smart = f"Name: {name_surname}\nEmail: {email}\nPhone: {phone_number}"
print(bio_smart)
