# Napisz funkcję, która przyjmuje listę ocen ucznia i zwraca jej średnią.

def f_calculate_avg_marks(marks_list):
    #    suma = 0
    #    for i in marks_list:
    #        suma = suma + i
    #   return suma / len(marks_list)
    return sum(marks_list) / len(marks_list)


marks_list = [5, 4, 2.5, 3]

print(f_calculate_avg_marks(marks_list))
