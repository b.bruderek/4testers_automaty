# Napisz funkcję, która przyjmuje listę liczb i zwraca trzy największe (artymetycznie)
# Przetestuj tę funkcję dla różnych “ciekawych” przypadków

def get_three_highest_numbers_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers, reverse=True)
    return sorted_list[0:3]

if __name__ == '__main__':
    list_of_numbers = [5, 145, -65, 74, 3248]
    print(get_three_highest_numbers_in_the_list(list_of_numbers))

